﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Security;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Timers;
using System.Xml.Linq;

namespace oly
{
    [ServiceContract(Namespace = "AOW", ProtectionLevel = ProtectionLevel.None)]
    public interface IOlyService
    {
        [OperationContract]
        string Login(UserItem value);

        [OperationContract]
        bool CheckVersion(string version);

        [OperationContract]
        string Logout(UserItem value);

        [OperationContract]
        string Register(UserItem value);

        [OperationContract]
        string UnRegister(UserItem value);

        [OperationContract]
        string Skip(UserItem value);

        [OperationContract]
        UpdateItem GetUpdates(UserItem value);
    }

    [DataContract]
    public class UserItem
    {
        private int _state = -1;

        [DataMember]
        public string UserName { get; set; }

        [DataMember]
        public string UserPass { get; set; }

        public void SetState(int s)
        {
            _state = s;
        }
    }

    [DataContract]
    public class UpdateItem
    {
        [DataMember]
        public Dictionary<int, UserItem> RegistrationList { get; set; }

        [DataMember]
        public List<UserItem> UserList { get; set; }
    }

    [ServiceBehavior(InstanceContextMode = InstanceContextMode.Single, ConcurrencyMode = ConcurrencyMode.Single)]
    public class OlyService : IOlyService
    {
        //public static readonly object _userLocker = new object();
        public static readonly object _regLocker = new object();
        private readonly Dictionary<string, DateTime> _activeUsers = new Dictionary<string, DateTime>();
        private readonly string _pathToUsersFile = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "users.xml");
        private readonly string _pathToVersionsFile = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "versions.xml");
        private readonly Timer _timer = new Timer(20000);
        private readonly Timer _usersTimer = new Timer(30000);

        private readonly Dictionary<string, string> _validUsers = new Dictionary<string, string>();

        public List<UserItem> QueueUserList = new List<UserItem>();
        public Dictionary<int, UserItem> RegistredUsers = new Dictionary<int, UserItem>();

        public List<UserItem> UserList = new List<UserItem>();
        private List<string> _supportedVersions = new List<string>(); 
        public OlyService()
        {
            _timer.Elapsed += _timer_Elapsed;
            _timer.Start();

            _usersTimer.Elapsed += _usersTimer_Elapsed;
            _usersTimer.Start();

            LoadUsers();
            LoadSupportedVersions();
        }

        public string Login(UserItem value)
        {
            lock (_regLocker)
            {
                string retVal = string.Empty;

                if (!_validUsers.ContainsKey(value.UserName))
                {
                    retVal = "Пользователь с таким логином не найден";

                    return retVal;
                }

                var found = _validUsers[value.UserName];
                if (found != value.UserPass)
                {
                    retVal = "Неверный пароль";

                    return retVal;
                }

                if (!UserList.Any(u => u.UserName == value.UserName) && !RegistredUsers.Values.Any(item => item.UserName == value.UserName))
                {
                    _activeUsers[value.UserName] = DateTime.Now;
                    UserList.Add(value);

                    retVal = "Успешный вход";
                }
                else
                {
                    retVal = string.Format("Пользователь {0} уже в системе", value.UserName);
                }
                
                return retVal;
            }
        }

        public bool CheckVersion(string version)
        {
            bool valid = false;
            lock (_regLocker)
            {
                try
                {
                    valid = _supportedVersions.Contains(version);
                }
                catch (Exception exception)
                {
                    valid = false;
                }
            }

            return valid;
        }

        private void LoadSupportedVersions()
        {
            lock (_regLocker)
            {
                try
                {
                    var versions = XDocument.Load(_pathToVersionsFile);
                    if (versions.Root != null)
                    {
                        foreach (var v in versions.Root.Elements("Version"))
                        {
                            var version = v.Value;
                            if (!string.IsNullOrEmpty(version))
                            {
                                _supportedVersions.Add(version);
                            }
                        }
                    }
                }
                catch (Exception)
                {

                }
            }
        }

        public string Logout(UserItem value)
        {
            lock (_regLocker)
            {
                var retVal = string.Empty;

                var foundUser = UserList.FirstOrDefault(u => u.UserName == value.UserName);
                var foundRegUser = RegistredUsers.FirstOrDefault(u => u.Value.UserName == value.UserName);

                _activeUsers.Remove(value.UserName);

                if (foundRegUser.Value != null)
                {
                    RegistredUsers.Remove(foundRegUser.Key);
                    retVal = "Вы удалены из очереди на регистрацию";
                }

                if (foundUser != null)
                {
                    UserList.Remove(foundUser);
                    retVal = "Вы успешно вышли из системы";
                }

                return retVal;
            }
        }

        public string Register(UserItem value)
        {
            lock (_regLocker)
            {
                string retVal;
                if (UserList.Any(u => u.UserName == value.UserName))
                {
                    int maxVal = 0;
                    if (RegistredUsers.Any() && RegistredUsers.Keys.Any())
                    {
                        maxVal = RegistredUsers.Keys.Max();
                    }
                   
                    RegistredUsers.Add(maxVal + 1, value);

                    var uAdd = UserList.FirstOrDefault(u => u.UserName == value.UserName);
                    UserList.Remove(uAdd);

                    retVal = "Регистрация";
                }
                else
                {
                    retVal = "Регистрация не удалась. Вы не найдены в списке активных пользователей";
                }

                return retVal;
            }
        }

        public string UnRegister(UserItem value)
        {
            lock (_regLocker)
            {
                string retVal;
                if (RegistredUsers.Values.Any(u => u.UserName == value.UserName))
                {
                    var userToRemove = RegistredUsers.FirstOrDefault(u => u.Value.UserName == value.UserName);

                    UserList.Add(userToRemove.Value);

                    RegistredUsers.Remove(userToRemove.Key);

                    retVal = "Удаление из регистрации";
                }
                else
                {
                    retVal = "Удаление из регистрации не удалось";
                }

                return retVal;
            }
        }

        public string Skip(UserItem value)
        {
            lock (_regLocker)
            {
                string retVal;
                if (RegistredUsers.Values.Any(u => u.UserName == value.UserName))
                {
                    var userToMove = RegistredUsers.FirstOrDefault(u => u.Value.UserName == value.UserName);
                    var keys = RegistredUsers.Keys.ToList();

                    var curInd = keys.IndexOf(userToMove.Key);
                    if (keys.Count > curInd + 1)
                    {
                        var nextKey = keys[curInd + 1];

                        if (RegistredUsers.ContainsKey(nextKey))
                        {
                            var switchUser = RegistredUsers[nextKey];

                            RegistredUsers[userToMove.Key] = switchUser;
                            RegistredUsers[nextKey] = userToMove.Value;

                            retVal = string.Format("Вы пропустили {0}", switchUser.UserName);
                        }
                        else
                        {
                            retVal = "Не удалось пропустить";
                        }
                    }
                    else
                    {
                        retVal = "Не удалось пропустить";
                    }
                }
                else
                {
                    retVal = "Ваш пользователь не найден";
                }

                return retVal;
            }
        }

        public UpdateItem GetUpdates(UserItem value)
        {
            lock (_regLocker)
            {
                if (_validUsers.ContainsKey(value.UserName) && (UserList.Any(i => i.UserName == value.UserName) || RegistredUsers.Values.Any(item => item.UserName == value.UserName)))
                {
                    _activeUsers[value.UserName] = DateTime.Now;

                    return new UpdateItem { RegistrationList = RegistredUsers, UserList = UserList };
                }
                else
                {
                    return new UpdateItem();
                }
                
            }
        }

        private void _usersTimer_Elapsed(object sender, ElapsedEventArgs e)
        {
            LoadUsers();
        }

        private void LoadUsers()
        {
            lock (_regLocker)
            {
                try
                {
                    var users = XDocument.Load(_pathToUsersFile);
                    if (users.Root != null)
                    {
                        _validUsers.Clear();
                        foreach (var user in users.Root.Elements("User"))
                        {
                            var attrName = user.Attribute("Name");
                            var attrPwd = user.Attribute("Password");

                            if (attrName != null && attrPwd != null)
                            {
                                _validUsers[attrName.Value] = attrPwd.Value;
                            }
                        }
                    }
                }
                catch (Exception)
                {
                    
                }
            }
        }

        private void _timer_Elapsed(object sender, ElapsedEventArgs e)
        {
            try
            {
                foreach (var user in _activeUsers)
                {
                    var userTime = user.Value.TimeOfDay.TotalSeconds;
                    var serverTime = DateTime.Now.TimeOfDay.TotalSeconds;

                    if (serverTime - userTime > 30)
                    {
                        Logout(new UserItem {UserName = user.Key});
                    }
                }

                var notActiveUsers = UserList.Where(u => !_activeUsers.ContainsKey(u.UserName));
                foreach (var na in notActiveUsers)
                {
                    Logout(new UserItem {UserName = na.UserName});
                }

                var notActiveRUsers = RegistredUsers.Values.Where(u => !_activeUsers.ContainsKey(u.UserName));
                foreach (var na in notActiveRUsers)
                {
                    Logout(new UserItem {UserName = na.UserName});
                }
            }
            catch (Exception)
            {
            }
        }
    }
}