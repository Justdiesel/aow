﻿using System;
using System.Threading;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Collections.Generic;
using System.IO;
using System.Diagnostics;
using System.Runtime.InteropServices;
namespace server
{

  
    public struct User
    {
        public string name;
        public string pass;
        public int sost;
        public void SetSost(int sost1)
        {
            sost = sost1;
        }
    }

    public class server
    {

        public List<User> users;
        public List<User> regusers;
        public List<User> ochered;
        public List<User> templist;
        ManualResetEvent tcpClientConnected = new ManualResetEvent(false);
        [DllImport("kernel32.dll", SetLastError = true)]
        [return: MarshalAs(UnmanagedType.Bool)]
        static extern bool AllocConsole();

        [DllImport("kernel32.dll", SetLastError = true)]
        [return: MarshalAs(UnmanagedType.Bool)]
        static extern bool FreeConsole();



        int kol = 0;
        public static string ulg1;
        public server()
        {


       users = new List<User>();
        regusers = new List<User>();
       ochered = new List<User>();
      templist  = new List<User>();

            var fileStream = new FileStream("acc.txt", FileMode.Open, FileAccess.Read);

            using (var streamReader = new StreamReader(fileStream, Encoding.UTF8))
            {
                string line;
                line = streamReader.ReadLine();
                kol = Convert.ToInt32(line);
                for (int i = 0; i < kol; i++)
                {           
                    User a;
                    line = streamReader.ReadLine();   
                    a.name = line;
                    line = streamReader.ReadLine();
                    a.pass = line;
                    a.sost = -1;
                    users.Add(a);
                }
                   
            }

        

     
 
}
        public void ProcessIncomingData(object obj)
        {
                TcpClient client = (TcpClient)obj;
                string sbs;
                byte[] data = new byte[1024];
                NetworkStream stream = null;
                stream = client.GetStream();
                StringBuilder builder = new StringBuilder();
                int bytes = 0;
                do
                {
                    bytes = stream.Read(data, 0, data.Length);
                    builder.Append(Encoding.Unicode.GetString(data, 0, bytes));
                }
                while (stream.DataAvailable);
                sbs = builder.ToString();
                string poz = sbs.Substring(0, 4);
                int i;
                if (poz == "log:")
                {

                    int ind = sbs.IndexOf("+");
                    string name = sbs.Substring(4, ind - 4);
                    string pass = sbs.Substring(ind + 1);
                    int nah = 0;
                    for (i = 0; i < users.Count; i++)
                    {
                        if (users[i].name == name && users[i].pass == pass)
                        {
                            nah = 1;
                            User temp = users[i];
                            temp.SetSost(0);
                            regusers.Add(temp);
                            users.RemoveAt(i);
                            i = users.Count;
                        }
                    }
                    if (nah == 0)
                    {
                    string reply = "Error!";                      
                    data = Encoding.Unicode.GetBytes(reply);             
                    stream.Write(data, 0, data.Length);
                   // Console.WriteLine(name + " not connected");
                    }
                    else
                    {
                        string reply = "+norm";
                        data = Encoding.Unicode.GetBytes(reply);
                        stream.Write(data, 0, data.Length);
                       //   Console.WriteLine(name + " connected");
                    }

                }
                                 
                if (poz == "get:")
                {
                    string str="";
                    for (i = 0; i < regusers.Count; i++)
                    {
                       
                            str += "&" + regusers[i].name;
                            str += "&" + regusers[i].sost;
                    }
                    for (i = 0; i < ochered.Count; i++)
                    {

                        str += "&" + ochered[i].name;
                        str += "&" + ochered[i].sost;
                    }
                    int sum;
                    string reply="";
                if (ulg1 == "")
                {
                    sum = regusers.Count + ochered.Count;
                    reply = "+set" + sum.ToString() + str;
                }
                else
                {
                    sum = regusers.Count + ochered.Count+1;
                    reply = "+set" + sum.ToString() + str+ulg1;
                }
                    data = Encoding.Unicode.GetBytes(reply);
                    stream.Write(data, 0, data.Length);
                 }
                if (poz == "reg:")
                {

                    int ind = sbs.IndexOf("+");
                    string name = sbs.Substring(4, ind - 4);
                    string pass = sbs.Substring(ind + 1);
                    for (i = 0; i < regusers.Count; i++)
                    {
                        if (regusers[i].name == name && regusers[i].pass == pass)
                        {
                            User temp = regusers[i];
                            temp.SetSost(1);
                            ochered.Add(temp);
                            regusers.RemoveAt(i);
                        }

                    }
                 //   Console.WriteLine(name + " registered");

                }

                if (poz == "prp:")
                {
                     if (ochered.Count > 1)
                        {
                            int ind = sbs.IndexOf("+");
                            string name = sbs.Substring(4, ind - 4);
                            string pass = sbs.Substring(ind + 1);

                            int currentPos = -1;
                            for (int j = 0; j < ochered.Count; j++)
                            {
                                if (ochered[j].name == name)
                                {
                                    currentPos = j;
                                }
                            }

                            if (currentPos >= 0 && currentPos < ochered.Count -1)
                            {
                                if (ochered.Count > 1)
                                {
                                    User temp = ochered[currentPos];
                                    ochered[currentPos] = ochered[currentPos+1];
                                    ochered[currentPos+1] = temp;
                                }
                            }


                            //if (ochered[0].name == name && ochered[0].pass == pass)
                            //{
                            //    if (ochered.Count > 1)
                            //    {
                            //        User temp = ochered[0];
                            //        ochered[0] = ochered[1];
                            //        ochered[1] = temp;
                            //    }
                            // //   Console.WriteLine(name + " missed registration");
                            //}
                        }
                }

                if (poz == "otm:")
                {

                    int ind = sbs.IndexOf("+");
                    string name = sbs.Substring(4, ind - 4);
                    string pass = sbs.Substring(ind + 1);
                    for (i = 0; i < ochered.Count; i++)
                    {
                        if (ochered[i].name == name && ochered[i].pass == pass)
                        {
                            User temp = ochered[i];
                            temp.SetSost(0);
                            regusers.Add(temp);
                            ochered.RemoveAt(i);
                        }

                    }
                 //   Console.WriteLine(name + " unregistered");
                }

                if (poz == "ulg:")
                {

                    int ind = sbs.IndexOf("+");
                    string name = sbs.Substring(4, ind - 4);
                    string pass = sbs.Substring(ind + 1);
                    for (i = 0; i < ochered.Count; i++)
                    {
                        if (ochered[i].name == name && ochered[i].pass == pass)
                        {
                            User temp = ochered[i];
                            temp.SetSost(0);
                            users.Add(temp);
                            ochered.RemoveAt(i);
                        }

                    }
                    for (i = 0; i < regusers.Count; i++)
                    {
                        if (regusers[i].name == name && regusers[i].pass == pass)
                        {
                            User temp = regusers[i];
                            temp.SetSost(0);
                            users.Add(temp);
                            regusers.RemoveAt(i);
                        }

                    }
                  //  Console.WriteLine(name + " disconected");
                               
            }
                client.Close();
            ulg1 = "";
        }

        void ProcessIncomingConnection(IAsyncResult ar)
        {
            TcpListener listener = (TcpListener)ar.AsyncState;
            TcpClient client = listener.EndAcceptTcpClient(ar);

            ThreadPool.QueueUserWorkItem(ProcessIncomingData, client);
            tcpClientConnected.Set();
        }

        public void start()
        {
            //IPEndPoint endpoint = new IPEndPoint(IPAddress.Parse("89.108.101.95"), 5000);
            IPEndPoint endpoint = new IPEndPoint(IPAddress.Parse("127.0.0.1"), 5001);
            TcpListener listener = new TcpListener(endpoint);
            listener.Start();

            while (true)
            {
                tcpClientConnected.Reset();
                listener.BeginAcceptTcpClient(new AsyncCallback(ProcessIncomingConnection), listener);
                tcpClientConnected.WaitOne();
            }
        }


        public void Save()
        {
            templist.Clear();
            templist.AddRange(users);
            templist.AddRange(regusers);
            templist.AddRange(ochered);
            var fileStream = new FileStream("acc.txt", FileMode.Create, FileAccess.Write);
            using (var streamWriter = new StreamWriter(fileStream, Encoding.UTF8))
            {
                streamWriter.WriteLine(templist.Count);            
                for (int i = 0; i < templist.Count; i++)
                {
                    streamWriter.WriteLine(templist[i].name);
                    streamWriter.WriteLine(templist[i].pass);              
                }

            }
        }

       public void Do()
        {
           Thread t = new Thread(start);
            t.Start();
        }
        public void Add(string name, string pass)
        {
            User temp;
            temp.name = name;
            temp.pass = pass;
            temp.sost = -1;
            users.Add(temp);
            Save();
        }
        void Delete(string name)
        {
            for (int i = 0; i < users.Count; i++)
            {
                if (users[i].name == name)
                {
                    users.RemoveAt(i);
                }
            }
            for (int i = 0; i < ochered.Count; i++)
            {
                if (ochered[i].name == name)
                {
                    ochered.RemoveAt(i);
                }
            }
            for (int i = 0; i < regusers.Count; i++)
            {
                if (regusers[i].name == name)
                {
                    regusers.RemoveAt(i);
                }
            }
            Save();
        }
        void ToOnline(string name)
        {
            for (int i = 0; i < ochered.Count; i++)
            {
                if (ochered[i].name == name)
                {
                    User temp=ochered[i];
                    temp.sost = 0;
                    regusers.Add(temp);
                    ochered.RemoveAt(i);
                }
            }
        }
        void ToExit(string name)
        {
            for (int i = 0; i < ochered.Count; i++)
            {
                if (ochered[i].name == name)
                {
                    User temp = ochered[i];
                    ulg1 = "&"+temp.name + "&" + "-1";
                    temp.sost = -1;
                    users.Add(temp);
                    ochered.RemoveAt(i);
                }
            }
            for (int i = 0; i < regusers.Count; i++)
            {
                if (regusers[i].name == name)
                {
                    User temp = regusers[i];
                    ulg1 = "&" + temp.name + "&" + "-1";
                    temp.sost = -1;
                    users.Add(temp);
                    regusers.RemoveAt(i);
                }
            }
        }

        static void Main(string[] args)
        {
            server s = new server();
            s.Do();

           

            while (true)
            {
                string str = Console.ReadLine();
                if (str == "add")
                {

                   

                    string name;
                    string pass;                
                    Console.Write("Введите ник: ");
                    name = Console.ReadLine();
                    Console.Write("Введите пароль: ");
                    pass = Console.ReadLine();
                    s.Add(name, pass);
                    Console.WriteLine("User "+ name+" added");                
                }
                if (str == "delete")
                {
                    string name;                
                    Console.Write("Введите ник: ");
                    name = Console.ReadLine();
                    s.Delete(name);
                    Console.WriteLine("User " + name + " deleted");
                }
                if (str == "toonline")
                {
                    string name;
                    Console.Write("Введите ник: ");
                    name = Console.ReadLine();
                    s.ToOnline(name);
                    Console.WriteLine("User " + name + " moved to online");
                }

                if (str == "toexit")
                {
                    string name;
                    Console.Write("Введите ник: ");
                    name = Console.ReadLine();
                    s.ToExit(name);
                    Console.WriteLine("User " + name + " exiting");
                }

            }

        }
    }

   
}
