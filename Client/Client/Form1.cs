﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Threading;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Windows.Forms;
using System.Media;
using System.IO;
using System.Configuration;
using System.Collections.Specialized;
namespace Client
{
    
    public partial class Form1 : Form
    {
        bool enableLog = false;
        string rez;
        int kol = 0;
        bool b = false;
        bool bb = true;
        public Form1()
        {
            InitializeComponent();

            try 
            {
                NameValueCollection appSettings = ConfigurationManager.AppSettings;
                var a = appSettings["enableLog"];

                if (a == "true")
                {
                    enableLog = true;
                }
            }
            catch(Exception e){
                handleException(e);
            }

            Application.ThreadException += Application_ThreadException;
        }

        void Login()
        {
            try
            {
                IPEndPoint ep = new IPEndPoint(IPAddress.Parse(Properties.Settings.Default.IP), Convert.ToInt32(Properties.Settings.Default.Port));
                TcpClient client = new TcpClient();
                client.Connect(ep);
                StringBuilder sb = new StringBuilder();
                using (NetworkStream stream = client.GetStream())
                {
                   string request = "log:" + textBox4.Text + "+" + textBox5.Text;
                    byte[] data = Encoding.Unicode.GetBytes(request);               
                    stream.Write(data, 0, data.Length);           
                    data = new byte[1024]; 
                    StringBuilder builder = new StringBuilder();
                    int bytes = 0;
                    do
                    {
                        bytes = stream.Read(data, 0, data.Length);
                        builder.Append(Encoding.Unicode.GetString(data, 0, bytes));
                    }
                    while (stream.DataAvailable);
                    rez = builder.ToString();
                    if (rez == "+norm")
                    {
                        var result = MessageBox.Show("Вы зашли!", "good", MessageBoxButtons.OK, MessageBoxIcon.Information);
                       label4.Visible = false;
                        label5.Visible = false;
                        textBox4.Visible = false;
                        textBox5.Visible = false;
                        button4.Visible = false;
                        textBox1.Visible = true;
                        textBox2.Visible = true;
                        textBox3.Visible = true;
                        label1.Visible = true;
                        label2.Visible = true;
                        label3.Visible = true;
                        button1.Visible = true;
                        button2.Visible = true;
                        button3.Visible = true;
                        timer1.Enabled = true;
                        button6.Visible = false;
                        MainMenuStrip.Visible = true;
                    }
                    else
                    {
                        var result = MessageBox.Show("Вы ввели неверный логин или пароль. Обратитесь к администратору", "Ошибка присоединения", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                }
                client.Close();
            }
            catch (SocketException se)
            {
                handleException(se);
                var result = MessageBox.Show("Не удалось подключится к серверу. Обратитесь к администратору", "Ошибка присоединения", MessageBoxButtons.OK, MessageBoxIcon.Error);            
            }
       
        }
        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void button4_Click(object sender, EventArgs e)
        {
            Login();       
        }

        private void Form1_FormClosed(object sender, FormClosedEventArgs e)
        {
            try
            {
                IPEndPoint ep = new IPEndPoint(IPAddress.Parse(Properties.Settings.Default.IP), Convert.ToInt32(Properties.Settings.Default.Port));
                TcpClient client = new TcpClient();
                client.Connect(ep);
                StringBuilder sb = new StringBuilder();
                using (NetworkStream stream = client.GetStream())
                {
                    string request = "ulg:" + textBox4.Text + "+" + textBox5.Text;
                    byte[] data = Encoding.Unicode.GetBytes(request);
                    stream.Write(data, 0, data.Length);
                }
                client.Close();
            }
            catch (SocketException se)
            {
                handleException(se);
                var result = MessageBox.Show("Не удалось отправить данные на сервер. Обратитесь к администратору", "Ошибка присоединения", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            try
            {
                int koloch = 0;
                textBox2.Text = "";
                textBox3.Text = "";
                IPEndPoint ep = new IPEndPoint(IPAddress.Parse(Properties.Settings.Default.IP), Convert.ToInt32(Properties.Settings.Default.Port));
                TcpClient client = new TcpClient();
                client.Connect(ep);
                int i;
                StringBuilder sb = new StringBuilder();
                using (NetworkStream stream = client.GetStream())         
                {
                    string request = "get:";
                    byte[] data = Encoding.Unicode.GetBytes(request);
                    stream.Write(data, 0, data.Length);
                    data = new byte[1024];
                    StringBuilder builder = new StringBuilder();
                    int bytes = 0;
                    do
                    {
                        bytes = stream.Read(data, 0, data.Length);
                        builder.Append(Encoding.Unicode.GetString(data, 0, bytes));
                    }
                    while (stream.DataAvailable);
                    rez = builder.ToString();
                    string rez1 = rez.Substring(0, 4);
                    if (rez1 == "+set")
                    {
                        rez = rez.Substring(4);
                        int index = rez.IndexOf("&");
                        string skol = rez.Substring(0, index);
                        kol = Convert.ToInt32(skol);
                        rez = rez.Substring(index + 1);
                        String[] substrings = rez.Split('&');
                        for (i = 0; i < kol; i++)
                        {
                            if (substrings[i * 2 + 1] == "-1")
                            {
                                if (substrings[i * 2] == textBox4.Text)
                                {
                                    timer1.Enabled = false;
                                    var result = MessageBox.Show("Вы отключены от сервера", "Отключены администратором", MessageBoxButtons.OK, MessageBoxIcon.Error);
                                    label4.Visible = true;
                                    label5.Visible = true;
                                    textBox4.Visible = true;
                                    textBox5.Visible = true;
                                    button4.Visible = true;
                                    textBox1.Visible = false;
                                    textBox2.Visible = false;
                                    textBox3.Visible = false;
                                    label1.Visible = false;
                                    label2.Visible = false;
                                    label3.Visible = false;
                                    button1.Visible = false;
                                    button2.Visible = false;
                                    button3.Visible = false;
                                    MainMenuStrip.Visible = false;
                                }
                              
                            }
                            if (substrings[i * 2 + 1] == "0")
                            {
                                textBox3.Text += substrings[i * 2];
                                textBox3.Text += Environment.NewLine;
                            }
                            if (substrings[i * 2 + 1] == "1")
                            {
                                textBox2.Text += substrings[i * 2];
                                textBox2.Text += Environment.NewLine;
                                koloch++;
                            }
                        }
                    }
                    
                }
                if (textBox2.Text != "")
                {
                    textBox1.Text = textBox2.Lines[0];
                    if (textBox1.Text != "")
                    {
                        if (textBox1.Text == textBox4.Text && b == false)
                        {
                             b = true;
                             SystemSounds.Asterisk.Play();
                             IntPtr handle = this.Handle;
                             if(bb==true)
                            FlashWindow1.FlashWindow(handle, FlashWindow1.FlashWindowFlags.FLASHW_ALL, 1, 1);
                        }
                        else
                        {
                            b = false;
                        }
                    }
                }
                   
                client.Close();
                if (koloch == 0)
                {
                    textBox1.Text = "";
                    textBox2.Text = "";
                }
                
            }
            catch (SocketException se)
            {
                timer1.Enabled = false;
                var result = MessageBox.Show("Не удалось отправить данные на сервер. Обратитесь к администратору", "Ошибка присоединения", MessageBoxButtons.OK, MessageBoxIcon.Error);            
                label4.Visible = true;
                label5.Visible = true;
                textBox4.Visible = true;
                textBox5.Visible = true;
                button4.Visible = true;
                textBox1.Visible = false;
                textBox2.Visible = false;
                textBox3.Visible = false;
                label1.Visible = false;
                label2.Visible = false;
                label3.Visible = false;
                button1.Visible = false;
                button2.Visible = false;
                button3.Visible = false;
                MainMenuStrip.Visible = false;
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            try
            {
                throw new SocketException();
                IPEndPoint ep = new IPEndPoint(IPAddress.Parse(Properties.Settings.Default.IP), Convert.ToInt32(Properties.Settings.Default.Port));
                TcpClient client = new TcpClient();
                client.Connect(ep);
                StringBuilder sb = new StringBuilder();
                using (NetworkStream stream = client.GetStream())
                {
                    string request = "reg:" + textBox4.Text + "+" + textBox5.Text;
                    byte[] data = Encoding.Unicode.GetBytes(request);
                    stream.Write(data, 0, data.Length);
                }
                client.Close();
            }
            catch (Exception se)
            {
                handleException(se);
                var result = MessageBox.Show("Не удалось отправить данные на сервер. Обратитесь к администратору", "Ошибка присоединения", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            try
            {
                textBox1.Text = "";
                IPEndPoint ep = new IPEndPoint(IPAddress.Parse(Properties.Settings.Default.IP), Convert.ToInt32(Properties.Settings.Default.Port));
                TcpClient client = new TcpClient();
                client.Connect(ep);
                StringBuilder sb = new StringBuilder();
                using (NetworkStream stream = client.GetStream())
                {
                    string request = "prp:" + textBox4.Text + "+" + textBox5.Text;
                    byte[] data = Encoding.Unicode.GetBytes(request);
                    stream.Write(data, 0, data.Length);
                }
                client.Close();
            }
            catch (SocketException se)
            {
                handleException(se);
                var result = MessageBox.Show("Не удалось отправить данные на сервер. Обратитесь к администратору", "Ошибка присоединения", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            textBox7.Text=Properties.Settings.Default.IP;
            textBox6.Text = Properties.Settings.Default.Port;
        }

        private void button3_Click(object sender, EventArgs e)
        {
            try
            {
                textBox1.Text = "";
                IPEndPoint ep = new IPEndPoint(IPAddress.Parse(Properties.Settings.Default.IP), Convert.ToInt32(Properties.Settings.Default.Port));
                TcpClient client = new TcpClient();
                client.Connect(ep);
                StringBuilder sb = new StringBuilder();
                using (NetworkStream stream = client.GetStream())
                {
                    string request = "otm:" + textBox4.Text + "+" + textBox5.Text;
                    byte[] data = Encoding.Unicode.GetBytes(request);
                    stream.Write(data, 0, data.Length);
                }
                client.Close();
            }
            catch (SocketException se)
            {
                handleException(se);
                var result = MessageBox.Show("Не удалось отправить данные на сревер. Обратитесь к администратору", "Ошибка присоединения", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void минимальныйToolStripMenuItem_Click(object sender, EventArgs e)
        {
            System.Windows.Forms.Control form = this;
            form.SuspendLayout();
            form.Width = 156;
            form.Height = 370;
            label3.Visible = false;
            textBox3.Visible = false;           
            textBox1.Width = 130;
            textBox2.Width = 130;
            button1.Left = 23;
            button2.Left = 23;
            button3.Left = 23;
            label1.Left = 36;
            label2.Left =45;
            textBox1.Left = 10;
            textBox2.Left = 10;
        
            button2.Top = 270;
            button3.Top = 300;
        }

        private void максимальныйToolStripMenuItem_Click(object sender, EventArgs e)
        {
            label3.Visible = true;
            textBox3.Visible = true;
            button3.Visible = true;
            System.Windows.Forms.Control form = this;
            form.SuspendLayout();
            form.Width = 374;
            form.Height = 326;
            textBox1.Width = 169;
            textBox2.Width = 169;
            label1.Left = 14;
            label2.Left = 14;
            button2.Left = 125;
            button3.Left = 236;
            button1.Left = 14;
            button2.Top = 239;
            button3.Top = 239;
        }

        private void button6_Click(object sender, EventArgs e)
        {
            textBox7.Text = Properties.Settings.Default.IP;
            textBox6.Text = Properties.Settings.Default.Port;
            textBox7.Visible = true;
            textBox6.Visible = true;
            label7.Visible = true;
            label6.Visible = true;
            button5.Visible = true;      
            button7.Visible = true;
            textBox4.Visible = false;
            textBox5.Visible = false;
            label5.Visible = false;
            label4.Visible = false;
            button4.Visible = false;
            button6.Visible = false;  
            MainMenuStrip.Visible = false;
           
        }

        private void button7_Click(object sender, EventArgs e)
        {
            textBox7.Visible = false;
            textBox6.Visible = false;
            label7.Visible = false;
            label6.Visible = false;
            button5.Visible = false;
            button7.Visible = false;
            textBox4.Visible = true;
            textBox5.Visible = true;
            label5.Visible = true;
            label4.Visible = true;
            button4.Visible = true;
            button6.Visible = true;

           
        }

        private void button5_Click(object sender, EventArgs e)
        {
            Properties.Settings.Default.IP=textBox7.Text;
            Properties.Settings.Default.Port=textBox6.Text;      
            Properties.Settings.Default.Save();
            timer1.Enabled = false;

            textBox7.Visible = false;
            textBox6.Visible = false;
            label7.Visible = false;
            label6.Visible = false;
            button5.Visible = false;
            button7.Visible = false;
            textBox4.Visible = true;
            textBox5.Visible = true;
            label5.Visible = true;
            label4.Visible = true;
            button4.Visible = true;
            button6.Visible = true;
        }

        private void button8_Click(object sender, EventArgs e)
        {
            textBox7.Visible = false;
            textBox6.Visible = false;
            label7.Visible = false;
            label6.Visible = false;
            button5.Visible = false;
            button1.Visible = true;
            button2.Visible = true;
            button3.Visible = true;
            button6.Visible = true;
            button7.Visible = false;
            textBox4.Visible = false;
            textBox5.Visible = false;
            label4.Visible = false;
            label5.Visible = false;
            label1.Visible = true;
            label2.Visible = true;
            label3.Visible = true;
            textBox1.Visible = true;
            textBox2.Visible = true;
            textBox3.Visible = true;
            MainMenuStrip.Visible = true;
        }

        private void поверхВсехОконToolStripMenuItem_Click(object sender, EventArgs e)
        {
            TopMost = true;
        }

        private void неПоверхВсехОконToolStripMenuItem_Click(object sender, EventArgs e)
        {
            TopMost = false;
        }

        private void отключитьМиганиеToolStripMenuItem_Click(object sender, EventArgs e)
        {
            bb = false;
        }

        private void включитьМиганиеToolStripMenuItem_Click(object sender, EventArgs e)
        {
            bb = true;
        }

        void Application_ThreadException(object sender, ThreadExceptionEventArgs e)
        {
            handleException(e.Exception);
        }

        private void handleException(Exception e)
        {
            if (!enableLog)
            {
                return;
            }

            if (!File.Exists("log.txt"))
            {
                using (var str = File.Create("log.txt"))
                {
                }
            }

            using (StreamWriter w = File.AppendText("log.txt"))
            {
                Log(e, w);
            }
        }

        public static void Log(Exception exc, TextWriter w)
        {
            w.Write("\r\nLog Entry : ");
            w.WriteLine("{0} {1}", DateTime.Now.ToLongTimeString(),
                DateTime.Now.ToLongDateString());
            w.WriteLine("  :");
            w.WriteLine("Message :{0}", exc.Message);
            w.WriteLine("InnerException :{0}", exc.InnerException);
            w.WriteLine("StackTrace :{0}", exc.StackTrace);
            w.WriteLine("-------------------------------");
        }
    }
}
