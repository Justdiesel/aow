﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Media;
using System.Runtime.CompilerServices;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;
using System.Timers;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Interop;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Threading;
using AowOlyClient.Annotations;
using AowOlyClient.ServiceReference1;

namespace AowOlyClient
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window, INotifyPropertyChanged
    {
        private readonly OlyServiceClient _client = new OlyServiceClient();
        private readonly UserItem _currentUser = new UserItem();
        private readonly Timer _timer;
        private readonly Timer _skipTimer1;
        private readonly Timer _skipTimer2;

        private bool _7secCount = false;
        private Int64 updatesCount = 0;
        private bool _flash = true;
        public ObservableCollection<UserItem> userList = new ObservableCollection<UserItem>();
        public ObservableCollection<UserItem> regUserList = new ObservableCollection<UserItem>();
       

        public ObservableCollection<UserItem> UserList
        {
            get
            {
                return userList;
            }
            set
            {
                userList = value;
                
            }
        }

        public ObservableCollection<UserItem> RegUserList
        {
            get
            {
                return regUserList;
            }
            set
            {
                regUserList = value;
            }
        }

        public bool UserRegistred
        {
            get
            {
                return _userRegistred;
            }
            set
            {
                _userRegistred = value;
                OnPropertyChanged("UserRegistred");
            }
        }

        private UserItem _currentRegUser;
        private bool _userRegistred;

        public UserItem CurrentRegUser
        {
            get { return _currentRegUser; }
            set
            {
                _currentRegUser = value;
                if (_currentRegUser != null)
                {
                    txtReg.Text = _currentRegUser.UserName;
                }
                else
                {
                    txtReg.Text = string.Empty;
                }
            }
        }

        public MainWindow(UserItem user)
        {
            UserRegistred = false;
            _currentUser = user;
            InitializeComponent();
            this.Title = this.Title + " - " + _currentUser.UserName;
            
            this.Closed += MainWindow_Closed;
            this.Loaded += MainWindow_Loaded;

            DataContext = this;

            _timer = new Timer();
            _timer.Interval = 1000;
            _timer.Elapsed += _timer_Elapsed;
           

            _skipTimer1 = new Timer();
            _skipTimer1.Interval = 7000;
            _skipTimer1.AutoReset = false;
            _skipTimer1.Elapsed += _skipTimer1_Elapsed;
        }

        void _skipTimer1_Elapsed(object sender, ElapsedEventArgs e)
        {
            Dispatcher.BeginInvoke(DispatcherPriority.Background, new Action(() =>
            {
                btnSkip.IsEnabled = true;
            }));
        }

        private void _timer_Elapsed(object sender, ElapsedEventArgs e)
        {
            try
            {
                if (_client.State == CommunicationState.Opened)
                {
                    var upd = _client.GetUpdates(_currentUser);

                    updatesCount++;
                    Dispatcher.BeginInvoke(DispatcherPriority.Background, new Action(() =>
                    {
                        servStat.Fill = Brushes.Green;

                        UserList.Clear();
                        if (upd.UserList != null)
                        {
                            foreach (var user in upd.UserList)
                            {
                                UserList.Add(user);
                            }
                        }
                        
                        RegUserList.Clear();
                        if (upd.RegistrationList != null)
                        {
                            foreach (var user in upd.RegistrationList.OrderBy(r => r.Key))
                            {
                                RegUserList.Add(user.Value);
                            }
                        }

                        if (RegUserList != null)
                        {
                            CurrentRegUser = RegUserList.FirstOrDefault();
                        }
                        
                        if (CurrentRegUser != null)
                        {
                            if (_flash && _currentUser.UserName == CurrentRegUser.UserName)
                            {
                                if (updatesCount%4 == 0)
                                {
                                    SystemSounds.Asterisk.Play();
                                }

                                var handle = new WindowInteropHelper(this).Handle;

                                FlashWindow1.FlashWindow(handle, FlashWindow1.FlashWindowFlags.FLASHW_ALL, 1, 1);
                            }
                        }
                    }));
                }
                else
                {
                    Dispatcher.BeginInvoke(DispatcherPriority.Background,
                        new Action(() => { servStat.Fill = Brushes.Red; }));
                }
            }
            catch (Exception exception)
            {
                if (_client != null)
                {
                    _client.Close();
                    _client.Abort();
                }

                if (_timer != null)
                {
                    _timer.Stop();
                    _timer.Enabled = false;
                }
                
                MessageBox.Show(exception.Message, "Сервер не доступен");
            }
        }

        void MainWindow_Loaded(object sender, RoutedEventArgs e)
        {
            try
            {
                _client.Open();
                var t = new Task(() =>
                {
                    Task.Delay(2000).Wait();
                    

                    _timer.Start();
                });
                t.Start();
               
                
            }
            catch (Exception exception)
            {
                MessageBox.Show(exception.Message, "Сервер не доступен");
            }
        }

        void MainWindow_Closed(object sender, EventArgs e)
        {
            Logout();
            
            try
            {
                _client.Close();
            }
            catch (Exception exception)
            {
                MessageBox.Show(exception.Message, "Сервер не доступен");
            }
        }

        private void Logout()
        {
            try
            {
                if (_client != null)
                {
                    if (_client.State == CommunicationState.Opened)
                    {
                        var message = _client.Logout(_currentUser);
                        txtStat.Text = string.Format("{0} - {1}", DateTime.Now.ToLongTimeString(), message);
                    }
                }
            }
            catch (Exception exception)
            {
                MessageBox.Show(exception.Message, "Сервер не доступен");
            }
        }

        private void BtnReg_OnClick(object sender, RoutedEventArgs e)
        {
            try
            {
                if (_client != null)
                {
                    if (_client.State == CommunicationState.Opened)
                    {
                        var message = _client.Register(_currentUser);
                        txtStat.Text = string.Format("{0} - {1}", DateTime.Now.ToLongTimeString(), message);
                        UserRegistred = true;
                    }
                }
            }
            catch (Exception exception)
            {
                MessageBox.Show(exception.Message, "Сервер не доступен");
            }
        }

        private void BtnSkip_OnClick(object sender, RoutedEventArgs e)
        {
            try
            {
                if (_client != null)
                {
                    if (_client.State == CommunicationState.Opened)
                    {
                        var message = _client.Skip(_currentUser);
                        txtStat.Text = string.Format("{0} - {1}", DateTime.Now.ToLongTimeString(), message);

                        if (message.StartsWith("Вы пропустили"))
                        {
                            _skipTimer1.Start();
                            btnSkip.IsEnabled = false;
                        }
                    }
                }
            }
            catch (Exception exception)
            {
                MessageBox.Show(exception.Message, "Сервер не доступен");
            }
        }

        private void BtnCancel_OnClick(object sender, RoutedEventArgs e)
        {
            try
            {
            if (_client != null)
            {
                if (_client.State == CommunicationState.Opened)
                {
                    var message = _client.UnRegister(_currentUser);
                    txtStat.Text = string.Format("{0} - {1}", DateTime.Now.ToLongTimeString(), message);
                    UserRegistred = false;
                }
            }
            }
            catch (Exception exception)
            {
                MessageBox.Show(exception.Message, "Сервер не доступен");
            }
        }

        private void MenuItemSound_OnChecked(object sender, RoutedEventArgs e)
        {
            _flash = mSound.IsChecked;
        }

        private void MenuItemTop_OnChecked(object sender, RoutedEventArgs e)
        {
            this.Topmost = mTop.IsChecked;
        }

        private void MenuItemMin_OnChecked(object sender, RoutedEventArgs e)
        {
            switchViewMode(mMin.IsChecked);
        }

        private void switchViewMode(bool isMinimalEnabled)
        {
            if (isMinimalEnabled)
            {
                rPanel.Visibility = Visibility.Collapsed;
                this.Width = 175;
                this.Height = 440;
                users.Height = 250;
                regQ.Height = 215;
                Grid.SetColumnSpan(lPanel, 2);
                Grid.SetRowSpan(pButtons, 2);
                pStatus.Visibility = Visibility.Collapsed;
                pButtons.Orientation = Orientation.Vertical;
            }
            else
            {
                rPanel.Visibility = Visibility.Visible;
                this.Width = 350;
                this.Height = 410;
                Grid.SetColumnSpan(lPanel, 1);
                Grid.SetRowSpan(pButtons, 1);
                users.Height = 235;
                regQ.Height = 200;
                pStatus.Visibility = Visibility.Visible;
                pButtons.Orientation = Orientation.Horizontal;
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;

        [NotifyPropertyChangedInvocator]
        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            var handler = PropertyChanged;
            if (handler != null) handler(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}
