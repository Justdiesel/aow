﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Xml.Linq;
using AowOlyClient.ServiceReference1;

namespace AowOlyClient
{
    /// <summary>
    /// Interaction logic for login.xaml
    /// </summary>
    public partial class login : Window
    {
        private OlyServiceClient _client = new OlyServiceClient();
        private string _version;
        public login()
        {
            InitializeComponent();
            
            Loaded += login_Loaded;
            Closed += login_Closed;
            _version = Assembly.GetEntryAssembly().GetName().Version.ToString();
        }

        void login_Loaded(object sender, RoutedEventArgs e)
        {
            try
            {
                _client.Open();

                if (!_client.CheckVersion(_version))
                {
                    MessageBox.Show("Запущена несовместимая версия!", "Ошибка");

                    Application.Current.Shutdown();
                }
            }
            catch (Exception exception)
            {
                MessageBox.Show(exception.Message, "Сервер не доступен");
            }
            try
            {
                var user = ConfigurationManager.AppSettings["user"];
                var password = ConfigurationManager.AppSettings["password"];

                if (user != null)
                {
                    txtLogin.Text = user;
                    txtPassword.Text = password;
                }
            }
            catch (Exception)
            {
                
            }
        }

        void login_Closed(object sender, EventArgs e)
        {
            _client.Close();
        }

        private void ButtonBase_OnClick(object sender, RoutedEventArgs e)
        {
            string result;

            try
            {
                result = _client.Login(new UserItem() { UserName = txtLogin.Text, UserPass = txtPassword.Text });
            }
            catch (Exception exception)
            {
                MessageBox.Show(exception.Message, "Сервер не доступен");
                return;
            }
            
            if (result.Contains("Успешный"))
            {
                if (chRemember.IsChecked.HasValue && chRemember.IsChecked.Value)
                {
                    try
                    {
                        SetSetting("user", txtLogin.Text);
                        SetSetting("password", txtPassword.Text);
                    }
                    catch (Exception)
                    {
                        
                    }
                }

                MessageBox.Show(result);
                MainWindow mw = new MainWindow(new UserItem{UserName = txtLogin.Text, UserPass = txtPassword.Text});
                mw.Show();

                this.Close();
            }
            else
            {
                MessageBox.Show(result);
            }
        }

        private void Login_OnPreviewKeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter)
            {
                ButtonBase_OnClick(null, null);
            }
        }

        internal static bool SetSetting(string Key, string Value)
        {
            bool result = false;
            try
            {
                System.Configuration.Configuration config =
                  ConfigurationManager.OpenExeConfiguration(
                                       ConfigurationUserLevel.None);

                config.AppSettings.Settings.Remove(Key);
                var kvElem = new KeyValueConfigurationElement(Key, Value);
                config.AppSettings.Settings.Add(kvElem);

                // Save the configuration file.
                config.Save(ConfigurationSaveMode.Modified);

                // Force a reload of a changed section.
                ConfigurationManager.RefreshSection("appSettings");

                result = true;
            }
            finally
            { }
            return result;
        } // function

        private void txtPassword_TextChanged(object sender, TextChangedEventArgs e)
        {

        }
    }
}
