[Setup]
AppId={{9A323AD5-C3AA-42C8-ADFD-4AC5E48E5ADC}
AppName=AoW Olympiad
AppVersion=1.3
DefaultDirName={userappdata}\AowOlympiad
DefaultGroupName=AoW Olympiad
UninstallDisplayIcon={app}\AowOlyClient.exe
Compression=lzma2
SolidCompression=yes
AppCopyright=Copyright (C) 2016 Angels of War, Inc.
AppPublisher=Angels of War, Inc
AppPublisherURL=http://angelsofwar.ru/
OutputBaseFilename=AowOlympiad

[Files]
Source: "AowOlyClient.exe"; DestDir: "{app}"; Permissions: everyone-full; Flags: ignoreversion
Source: "AowOlyClient.exe.config"; DestDir: "{app}"; Permissions: everyone-full; Flags: ignoreversion
Source: "ClientOld.exe"; DestDir: "{app}"; Permissions: everyone-full; Flags: ignoreversion

[Icons]
Name: "{group}\AoW Olympiad v2"; Filename: "{app}\AowOlyClient.exe"
Name: "{group}\�������� ���������"; Filename: "{uninstallexe}"
Name: "{group}\Olympiad Old"; Filename: "{app}\ClientOld.exe"
Name: "{commondesktop}\AoW Olympiad"; Filename: "{app}\AowOlyClient.exe"; Tasks: desktopicon

[Tasks]
Name: "desktopicon"; Description: "{cm:CreateDesktopIcon}"; GroupDescription: "{cm:AdditionalIcons}"; Flags: checkablealone

[Languages]
Name: "russian"; MessagesFile: "compiler:Languages\Russian.isl"; LicenseFile: "License_RUS.txt"